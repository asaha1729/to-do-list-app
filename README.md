
# To Do App

This is simple Todo Application with basic CRUD functionalities.

## Getting Started

You can add a new user and post new To Do Items. 

### Prerequisites

The technologies used to make this app are>

> * Node.js
> * PostgreSQL (using Sequalize as ORM)
> * Basic React.js

### Run

The project is run using the following command.
```
npm run dev-start
```
### Usage Steps
>* Add a User
>*  Add a Todo Title.
>* Add content and due date to each Todo main item.

### Database Architecture
There are 3 Tables.
```
Users
Todos
TodoItems
```
![To Do Database Architecture](./Todo_db_diag.jpg)

## Authors
* Asmita Saha asaha1729@gmail.com
