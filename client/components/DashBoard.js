import React from 'react';
import moment from 'moment'
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import { InputGroup, FormControl, Button, ListGroup, Card, ButtonGroup } from 'react-bootstrap';


export default class DashBoard extends React.Component {
  constructor(props) {
    super();
    this.state = {
      userDetails: props.userDetails,
      userId: props.userDetails.id,
      todo: '',
      todoContent: '',
      todoDuedate: new Date().toDateString(),
      todoList: []
    }
  }
  componentDidMount() {
    this.getTodoList()
  }
  addToDo = () => {
    if (!!this.state.todo) {
      const url = '/api/users/' + this.state.userId + '/todos'
      axios.post(url, { title: this.state.todo })
        .then(resp => {
          this.setState({ todo: '' })
          this.getTodoList()
        })
        .catch(e => console.log(e))
    }
  }

  deleteToDo = (id) => {
    const url = '/api/users/' + this.state.userId + '/todos/'
    axios.delete(url + id)
      .then(resp => {
        this.getTodoList()
      })
      .catch(e => console.log(e))
  }

  deleteToDoItem = (id, todoId) => {
    const url = '/api/users/' + this.state.userId + '/todos/'
    axios.delete(url + todoId + '/items/' + id)
      .then(resp => {
        this.getTodoList()
      })
      .catch(e => console.log(e))
  }

  addToDoItem = (id) => {
    let d = new Date(this.state["todoDuedate_" + id]).toDateString()
    const url = '/api/users/' + this.state.userId + '/todos/'
    axios.post(url + id + '/items', {
      content: this.state["todoContent_" + id],
      dueDate: new Date(this.state["todoDuedate_" + id]).toDateString(),
      success: false,
    })
      .then(resp => {
        this.setState({ 
          ["todoContent_" + id]: "",
          ["todoDuedate_" + id]: ""
        })
        this.getTodoList()
      })
      .catch(e => console.log(e))
  }

  editToDoItem = (item, id, todoId) => {
    if(!this.state["todoContent_" + todoId + "_" + id] && !!this.state["todoDuedate_" + todoId + "_" + id]) {
      item.dueDate = new Date(this.state["todoDuedate_" + todoId + "_" + id]).toDateString()
    } else if (!this.state["todoDuedate_" + todoId + "_" + id] && !!this.state["todoContent_" + todoId + "_" + id]){
      item.content = this.state["todoContent_" + todoId + "_" + id]
    }
    const url = '/api/users/' + this.state.userId + '/todos/'
    axios.post(url + todoId + '/items/' + id + '/update', {
      content: item.content,
      dueDate: item.dueDate,
      success: false,
    })
      .then(resp => {
        this.setState({
          ["todoContent_" + todoId + "_" + id]: '',
          ["todoDuedate_" + todoId + "_" + id]: ''
        })
        this.getTodoList()
      })
      .catch(e => console.log(e))
  }

  getTodoList = () => {
    const url = '/api/users/' + this.state.userId + '/todos'
    axios.get(url)
      .then(resp => this.setState({ todoList: resp.data }))
      .catch(e => console.log(e))
  }

  formatDate = (d) => {
    return moment(d).format('YYYY-MM-DD')
  }

  onValueChange = (name, value) => {
    this.setState({
      [name]: value
    })
  }

  inputData = (todoId, item) => {
    return (
      <ListGroup.Item style={{border: '0px'}} className="p-0">
        {
          <InputGroup className="mb-1">
            <FormControl
              placeholder="Content"
              value={
                item
                  ? this.state['todoContent_' + todoId + '_' + item.id] || item.content
                  : this.state["todoContent_" + todoId]
              }
              onChange={(e) => item
                ? this.onValueChange('todoContent_' + todoId + '_' + item.id, e.target.value)
                : this.onValueChange('todoContent_' + todoId, e.target.value)
              }
            />
            <FormControl
              type="date"
              placeholder="Duedate"
              aria-describedby="basic-addon1"
              value={
                item
                  ? this.state['todoDuedate_' + todoId + '_' + item.id] || this.formatDate(item.dueDate)
                  : this.state["todoDuedate" + todoId]}
              onChange={(e) => item
                ? this.onValueChange('todoDuedate_' + todoId + '_' + item.id, e.target.value)
                : this.onValueChange('todoDuedate_' + todoId, e.target.value)
              }
            />
            {
              item
                ?
                <ButtonGroup className="float-right">
                  <Button onClick={() => this.editToDoItem(item, item.id, todoId)} >Edit</Button>
                  <Button onClick={() => this.deleteToDoItem(item.id, todoId)} >Delete</Button>
                </ButtonGroup>
                : <Button onClick={() => this.addToDoItem(todoId)} >Add New</Button>
            }
          </InputGroup>
        }
      </ListGroup.Item>
    )
  }

  render() {
    return (
      <div>
          <InputGroup className="mx-auto my-2 w-50">
            <FormControl
              placeholder="New ToDo Title"
              value={this.state.todo}
              onChange={(e) => this.onValueChange('todo', e.target.value)}
            />
            <Button onClick={this.addToDo} >Add</Button>
          </InputGroup>
            <div>
              {
                this.state.todoList.map(todo =>
                  <Card body className="mb-2">
                    <Card.Title className="mb-3 mx-2">
                      {todo.title}
                      <Button className="float-right" onClick={() => this.deleteToDo(todo.id)} >Delete</Button>
                    </Card.Title>
                      <div className="w-100">
                        {
                          todo.TodoItems.map(item => this.inputData(todo.id, item) )
                        }
                        <br/>
                        {
                          this.inputData(todo.id)
                        }
                      </div>
                  </Card>
                )
              }
            </div>
      </div>
    )
  }
}