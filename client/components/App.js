import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import User from './User'
import DashBoard from './DashBoard'
import { Button, ButtonGroup, Card } from 'react-bootstrap';


export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      firstUser: true,
      showDash: false,
      userDetails: null
    }
  }
  getUserDetails = (data) => {
    if (!isNaN(data.id)) {
      this.setState({
        userDetails: data,
        showDash: true
      })
    } else {
      window.alert('Invalid User')
    }
  }
  logout = () => {
    this.setState({ showDash: false, userDetails: null })
  }
  render() {
    return (
      <Card className="mx-auto" style={{ height: 'auto', width: '70vw' }}>
        <Card.Header className="clearfix mb-1 p-3">
          <h3 className="float-left">To Do APP</h3>
          <br />
          {
            !this.state.showDash
              ? null
              :
              <ButtonGroup className="float-right">
                <Button onClick={() => this.setState({ showDash: false })}>User</Button>
                <Button onClick={this.logout}>Logout</Button>
              </ButtonGroup>
          }
        </Card.Header>
        <Card.Body className="p-3">
        {
          this.state.showDash
            ? <DashBoard userDetails={this.state.userDetails} />
            : <User
              userDetails={this.state.userDetails}
              userFound={this.getUserDetails}
              userDeleted={this.logout}
            />
        }
        </Card.Body>
      </Card>
    );
  }
}