import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import { InputGroup, FormControl, Button, ListGroup, Card, ButtonGroup } from 'react-bootstrap';


export default class User extends React.Component {
    constructor() {
        super();
        this.state = {
            userName: '',
            password: ''
        }
    }
    componentWillMount() {
        if (this.props.userDetails) {
            this.setState({
                userName: this.props.userDetails.username,
                password: this.props.userDetails.password
            })
        }
    }
    componentWillReceiveProps(newProps) {
        if (newProps.userDetails) {
            this.setState({
                userName: newProps.userDetails.userName,
                password: newProps.userDetails.password
            })
        }
    }
    checkUser = () => {
        const { userName, password } = this.state
        if (!!userName && !!password) {
            if (this.props.userDetails) {
                axios.post('/api/users/' + this.props.userDetails.id, { userName, password })
                    .then((resp) => console.log(resp))
                    .catch(e => console.log(e))
            }
            axios.post('/api/users/check', { userName, password })
                .then((resp) => this.props.userFound(resp.data))
                .catch(e => console.log(e))
        }
    }
    createUser = () => {
        const { userName, password } = this.state
        if (!!userName && !!password) {
            axios.post('/api/users', { userName, password })
                .then((resp) => this.props.userFound(resp.data))
                .catch(e => console.log(e))
        }
    }
    deleteUser = () => {
        const { userName, password } = this.state
        if (!!userName && !!password) {
            axios.delete('/api/users/'+this.props.userDetails.id)
                .then((resp) => this.props.userDeleted(resp.data))
                .catch(e => console.log(e))
        }
    }
    onValueChange = (name, value) => {
        this.setState({
            [name]: value
        })
    }
    render() {
        return (
            <div>
                <Card body>
                    <Card.Title>User Login</Card.Title>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="User Name"
                            aria-describedby="basic-addon1"
                            value={this.state.userName}
                            onChange={(e) => this.onValueChange('userName', e.target.value)}
                        />
                        <FormControl
                            placeholder="Password"
                            type="password"
                            aria-describedby="basic-addon1"
                            value={this.state.password}
                            onChange={(e) => this.onValueChange('password', e.target.value)}
                        />
                        {
                            this.props.userDetails
                                ?
                                <ButtonGroup>
                                    <Button onClick={this.checkUser} >Update</Button>
                                    <Button onClick={this.deleteUser} >Delete</Button>
                                    <Button onClick={() => this.props.userFound(this.props.userDetails)} >Back</Button>
                                </ButtonGroup>
                                :
                                <ButtonGroup>
                                    <Button onClick={this.checkUser} >Login</Button>
                                    <Button onClick={this.createUser} >Add User</Button>
                                </ButtonGroup>
                        }
                    </InputGroup>
                </Card>
            </div>
        )
    }
}
