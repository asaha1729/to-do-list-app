// server.js
import express from 'express';
var path = require('path');

const app = express()
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './client'));
app.use(express.static(path.join(__dirname, './client')));

app.use(express.json()) //middleware for parsing request body
require('./server/routes')(app);

app.get('/', (req, res) => {
  return res.render('index');
})

app.listen(3000)
console.log('app running on port ', 3000);