'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  Users.associate = function(models) {
    // associations can be defined here
    Users.hasMany(models.Todo, {
      foreignKey: 'user_id',
      as: 'Todos',
      onDelete: 'CASCADE',
      hooks:true,
      constraints: true
    });
  };
  return Users;
};