'use strict';
module.exports = (sequelize, DataTypes) => {
  const TodoItem = sequelize.define('TodoItem', {
    content: DataTypes.TEXT,
    dueDate: DataTypes.DATE,
    success: DataTypes.BOOLEAN,
    todo_id: DataTypes.INTEGER
  }, {});
  TodoItem.associate = function(models) {
    // associations can be defined here
    TodoItem.belongsTo(models.Todo, {
      foreignKey: 'todo_id',
      as: 'Todos'
    })
  };
  return TodoItem;
};