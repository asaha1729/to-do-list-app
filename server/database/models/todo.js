'use strict';
module.exports = (sequelize, DataTypes) => {
  const Todo = sequelize.define('Todo', {
    title: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {});
  Todo.associate = function(models) {
    // associations can be defined here
    Todo.hasMany(models.TodoItem, {
      foreignKey: 'todo_id',
      as: 'TodoItems',
      onDelete: 'CASCADE',
      hooks:true
    });
    Todo.belongsTo(models.Users, {
      foreignKey: 'user_id',
      as: 'Users'
    })
  };
  return Todo;
};