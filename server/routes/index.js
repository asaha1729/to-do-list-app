const usersController = require('../controllers').users
const todosController = require('../controllers').todoList;
const todoItemsController = require('../controllers').todoItem;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the Todos API!',
  }));

  app.post('/api/users', usersController.create);
  app.post('/api/users/check', usersController.checkUser);
  app.post('/api/users/:userId', usersController.updateUser);
  app.delete('/api/users/:userId', usersController.delete);


  app.post('/api/users/:userId/todos', todosController.create);
  app.get('/api/users/:userId/todos', todosController.getAll);
  app.delete('/api/users/:userId/todos/:todoId', todosController.delete);

  app.post('/api/users/:userId/todos/:todoId/items', todoItemsController.create);
  app.post('/api/users/:userId/todos/:todoId/items/:todoItemId/update', todoItemsController.update);
  app.delete('/api/users/:userId/todos/:todoId/items/:todoItemId', todoItemsController.delete);
};