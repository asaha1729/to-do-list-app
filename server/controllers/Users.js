const Users = require('../database/models').Users;

module.exports = {
  create(req, res) {
    return Users
      .create({
        username: req.body.userName,
        password: req.body.password
      })
      .then(user => res.status(201).send(user))
      .catch(error => res.status(400).send(error));
  },
  checkUser(req, res) {
    return Users
      .findOne({
        where: {
        username: req.body.userName,
        password: req.body.password
        }
      })
      .then(user => res.status(201).send(user))
      .catch(error => res.status(400).send(error));
  },
  updateUser(req, res) {
    return Users
      .update({
        username: req.body.userName,
        password: req.body.password
      },
      {
        where: {
          id: parseInt(req.params.userId),
        }
      })
      .then(user => res.status(201).send(user))
      .catch(error => res.status(400).send(error));
  },
  delete(req, res) {
    return Users
      .destroy({
        where: {
          id: parseInt(req.params.userId),
        }
      })
      .then(function (deletedRecord) {
        if (deletedRecord === 1) {
          res.status(200).json({ message: "Deleted successfully" });
        }
        else {
          res.status(404).json({ message: "record not found" })
        }
      })
      .catch(function (error) {
        res.status(500).json(error);
      });
  },
};