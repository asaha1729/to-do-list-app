const TodoItem = require('../database/models').TodoItem;

module.exports = {
  create(req, res) {
    return TodoItem
      .create({
        content: req.body.content,
        dueDate: req.body.dueDate,
        success: req.body.success,
        todo_id: req.params.todoId
      })
      .then(todoItem => res.status(201).send(todoItem))
      .catch(error => res.status(400).send(error));
  },
  update(req, res) {
    return TodoItem
      .update({
        content: req.body.content,
        dueDate: req.body.dueDate,
        success: req.body.success,
        todo_id: req.params.todoId
      },
      {
        where: {
          id: parseInt(req.params.todoItemId),
          todo_id: parseInt(req.params.todoId)
        }
      })
      .then(todoItem => res.status(201).send(todoItem))
      .catch(error => res.status(400).send(error));
  },
  delete(req, res) {
    return TodoItem
      .destroy({
        where: {
          id: parseInt(req.params.todoItemId),
          todo_id: parseInt(req.params.todoId)
        }
      })
      .then(function (deletedRecord) {
        if (deletedRecord === 1) {
          res.status(200).json({ message: "Deleted successfully" });
        }
        else {
          res.status(404).json({ message: "record not found" })
        }
      })
      .catch(function (error) {
        res.status(500).json(error);
      });
  },
};