const Todo = require('../database/models').Todo;
const TodoItem = require('../database/models').TodoItem;


module.exports = {
  create(req, res) {
    return Todo
      .create({
        title: req.body.title,
        user_id: req.params.userId
      })
      .then(todo => res.status(201).send(todo))
      .catch(error => {
        console.log(error)
        res.status(400).send(error)
      });
  },
  getAll(req, res) {
    return Todo
      .findAll({
        where: {
          user_id: parseInt(req.params.userId)
        },
        include: [{
          model: TodoItem,
          as: 'TodoItems'
        }]
      })
      .then(todos => res.status(200).send(todos))
      .catch(error => res.status(400).send(error));
  },
  delete(req, res) {
    console.log("????", req.params)
    return Todo
      .destroy({
        where: {
          id: req.params.todoId,
          user_id: req.params.userId
        }
      })
      .then(function (deletedRecord) {
        console.log("????", deletedRecord)
        if (deletedRecord === 1) {
          res.status(200).json({ message: "Deleted successfully" });
        }
        else {
          res.status(404).json({ message: "record not found" })
        }
      })
      .catch(function (error) {
        res.status(500).json(error);
      });
  },
};